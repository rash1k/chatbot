package ua.com.rash1k.chatbot.chatbot.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QuickReply {
    @SerializedName("content_type")
    @Expose
    private String contentType;
    @SerializedName("payload")
    @Expose
    private String payload;
    @SerializedName("title")
    @Expose
    private String title;

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
