package ua.com.rash1k.chatbot.chatbot;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.squareup.picasso.Picasso;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.messages.MessagesListAdapter;
import com.stfalcon.chatkit.utils.DateFormatter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import ua.com.rash1k.chatbot.R;
import ua.com.rash1k.chatbot.chatbot.fixtures.MessagesFixtures;
import ua.com.rash1k.chatbot.chatbot.model.Message;
import ua.com.rash1k.chatbot.chatbot.utils.AppUtils;


public abstract class BaseMessageFragment extends Fragment
        implements MessagesListAdapter.SelectionListener,
        MessagesListAdapter.OnLoadMoreListener,
        DateFormatter.Formatter {

    private static final int TOTAL_MESSAGES_COUNT = 100;

    protected final String mSenderId = "0";
    protected ImageLoader mImageLoader;
    protected MessagesListAdapter<Message> mMessagesAdapter;

    private Menu menu;
    private int selectionCount;
    private Date lastLoadedDate;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mImageLoader = (imageView, url) -> {
            if (AppUtils.isStringHttpProtocol(url)) {
                Uri uri = Uri.parse(url);
                Picasso.get()
                        .load(uri)
                        .into(imageView);
            }
            //else {
              //  Picasso.get()
                //        .load(R.drawable.chat_bot)
                  //      .into(imageView);
            //}
        };
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        this.menu = menu;
        inflater.inflate(R.menu.chat_actions_menu, menu);
        onSelectionChanged(0);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                mMessagesAdapter.deleteSelectedMessages();
                break;
            case R.id.action_copy:
                mMessagesAdapter.copySelectedMessagesText(Objects.requireNonNull(getContext()),
                        getMessageStringFormatter(), true);
                AppUtils.showToast(getContext(), R.string.copied_message, true);
                break;
        }
        return true;
    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        if (totalItemsCount < TOTAL_MESSAGES_COUNT) {
            loadMessages();
        }
    }

    @Override
    public void onSelectionChanged(int count) {
        this.selectionCount = count;
        menu.findItem(R.id.action_delete).setVisible(count > 0);
        menu.findItem(R.id.action_copy).setVisible(count > 0);
    }

    protected void loadMessages() {
        new Handler().postDelayed(new Runnable() { //imitation of internet connection
            @Override
            public void run() {
                ArrayList<Message> messages = MessagesFixtures.getMessages(lastLoadedDate);
                lastLoadedDate = messages.get(messages.size() - 1).getCreatedAt();
//                mMessagesAdapter.addToEnd(messages, false);
            }
        }, 1000);
    }

    protected MessagesListAdapter.Formatter<Message> getMessageStringFormatter() {
        return new MessagesListAdapter.Formatter<Message>() {
            @Override
            public String format(Message message) {
                String createdAt = new SimpleDateFormat("MMM d, EEE 'at' h:mm a", Locale.getDefault())
                        .format(message.getCreatedAt());

                String text = message.getText();
                if (text == null) text = "[attachment]";

                return String.format(Locale.getDefault(), "%s: %s (%s)",
                        message.getUser().getName(), text, createdAt);
            }
        };
    }

    @Override
    public String format(Date date) {
        if (DateFormatter.isToday(date)) {
            return getString(R.string.date_header_today);
        } else if (DateFormatter.isYesterday(date)) {
            return getString(R.string.date_header_yesterday);
        } else {
            return DateFormatter.format(date, DateFormatter.Template.STRING_DAY_MONTH_YEAR);
        }
    }
}
