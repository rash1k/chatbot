package ua.com.rash1k.chatbot.chatbot.utils;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.StringRes;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import java.util.Objects;

/*
 * Created by troy379 on 04.04.17.
 */
public class AppUtils {

    public static void showToast(Context context, @StringRes int text, boolean isLong) {
        showToast(context, context.getString(text), isLong);
    }

    public static void showToast(Context context, String text, boolean isLong) {
        Toast.makeText(context, text, isLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();
    }

    public static void hideKeyboard(Activity act){
        InputMethodManager imm = (InputMethodManager)
                act.getSystemService(Activity.INPUT_METHOD_SERVICE);
        Objects.requireNonNull(imm).hideSoftInputFromWindow(Objects.requireNonNull(act.getCurrentFocus()).getWindowToken(),0);
    }


    public static boolean isStringHttpProtocol(String url){
        return url.startsWith("http");
    }
}