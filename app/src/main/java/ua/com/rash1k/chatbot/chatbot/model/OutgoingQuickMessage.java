package ua.com.rash1k.chatbot.chatbot.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OutgoingQuickMessage extends Message {
    @SerializedName("payload")
    @Expose
    private String payload;


    public String getPayload() {
        return payload;
    }


    public void setPayload(String payload) {
        this.payload = payload;
    }
}
