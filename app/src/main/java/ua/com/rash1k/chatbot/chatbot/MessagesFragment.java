package ua.com.rash1k.chatbot.chatbot;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import com.google.android.flexbox.FlexboxLayout;
import com.stfalcon.chatkit.messages.MessageHolders;
import com.stfalcon.chatkit.messages.MessageInput;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;
import com.udevel.widgetlab.TypingIndicatorView;

import fisk.chipcloud.ChipCloud;
import fisk.chipcloud.ChipCloudConfig;
import ua.com.rash1k.chatbot.R;
import ua.com.rash1k.chatbot.chatbot.model.IncomingMessage;
import ua.com.rash1k.chatbot.chatbot.model.Message;
import ua.com.rash1k.chatbot.chatbot.utils.AppUtils;
import ua.com.rash1k.chatbot.chatbot.utils.MessageUtils;

public class MessagesFragment extends BaseMessageFragment implements
        MessageInput.InputListener,
        ChatBot.OnMessageListener {

    //    Message.OnChipListener,
    //    MessageInput.AttachmentsListener,

    public static final String TAG = "MessageFragment";
    public static final String TEMP_USER_NAME = "9ooQeGTQrG";
    //public static final String TEMP_USER_ID = TEMP_USER_NAME;
    public static final String TEMP_USER_ID = "1";


    private ChatBot mChatBot;
    private MessagesList mMessagesList;
    private ChipCloud mChipCloud;
    private FlexboxLayout mFlexBox;
    private MessageInput mMessageInput;
    private TypingIndicatorView mTypingIndicatorView;

    public static Fragment newInstance() {
        return new MessagesFragment();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mChatBot = new ChatBot(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_styled_messages, container,
                false);
        mTypingIndicatorView = view.findViewById(R.id.typingIndicator);
        mMessagesList = view.findViewById(R.id.messagesList);

        initMessageInput(view);
        initChipCloud(view);
        initAdapter();

        return view;
    }


    private void initMessageInput(View view) {
        mMessageInput = view.findViewById(R.id.input);
        hideKeyboardWhenMessageInputEmpty();
        mMessageInput.setInputListener(this);
    }

    private void hideKeyboardWhenMessageInputEmpty(){
            EditText editText = mMessageInput.getInputEditText();

           editText.addTextChangedListener(new TextWatcher() {
               @Override
               public void beforeTextChanged(CharSequence s, int start, int count, int after) {

               }

               @Override
               public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if(s.length() == 0){
                        mMessageInput.getInputEditText().onEditorAction(EditorInfo.IME_ACTION_DONE);
                    }
               }

               @Override
               public void afterTextChanged(Editable s) {

               }
           });
    }

    private void initChipCloud(View view) {
        mFlexBox = view.findViewById(R.id.flexbox);

        Resources res = getResources();

        int closeAnimPeriod = 500;
        ChipCloudConfig withCloseConfig = new ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.single)
                .checkedChipColor(res.getColor(R.color.selected_color))
                .checkedTextColor(res.getColor(R.color.selected_font_color))
                .uncheckedChipColor(res.getColor(R.color.accent_light_new))
                .uncheckedTextColor(res.getColor(R.color.white))
                .showClose(res.getColor(R.color.accent_dark), closeAnimPeriod);

        mChipCloud = new ChipCloud(getContext(), mFlexBox, withCloseConfig);
    }




    @Override
    public boolean onSubmit(CharSequence input) {
        String text = input.toString();
        mMessagesAdapter.addToStart(
                MessageUtils.getTextMessage(TEMP_USER_ID,
                TEMP_USER_NAME, text), true);

        sendOutgoingMessage(text);

        AppUtils.hideKeyboard(getActivity());

        showTypingIndicator();

        return true;
    }

    private void showTypingIndicator() {
        mTypingIndicatorView.setVisibility(View.VISIBLE);
    }


    @Override
    public void onMessage(final String json) {
        Log.d(TAG, "onMessage: " + json);
        final boolean isTypeMessage = MessageUtils.getMessageType(json);
        final IncomingMessage message = MessageUtils.createMessageFromJson(json);

        getActivity().runOnUiThread(() -> {
            if (message != null && isTypeMessage) {
                hideTypingIndicator();
                mMessagesAdapter.addToStart(message, true);
                showChipsCloud(message);
            }
        });
    }
    private void hideTypingIndicator() {
        mTypingIndicatorView.setVisibility(View.INVISIBLE);
    }

    private void initAdapter() {
        MessageHolders holdersConfig = new MessageHolders()
                .setOutcomingImageLayout(R.layout.item_custom_outcoming_image_message)
                .setIncomingTextLayout(R.layout.item_custom_incoming_text_message)
                .setOutcomingTextLayout(R.layout.item_custom_outcoming_text_message)
                .setIncomingImageLayout(R.layout.item_custom_incoming_image_message);


        super.mMessagesAdapter = new MessagesListAdapter<>("1", holdersConfig, super.mImageLoader);
        super.mMessagesAdapter.setLoadMoreListener(this);
        super.mMessagesAdapter.enableSelectionMode(this);
        super.mMessagesAdapter.setDateHeadersFormatter(this);
        mMessagesList.setAdapter(super.mMessagesAdapter);
    }

    private void showChipsCloud(final IncomingMessage message) {
        String[] chipsArrayLabel = MessageUtils.getChipsArrayLabel(message);
        if (chipsArrayLabel != null && chipsArrayLabel.length > 0) {
            mChipCloud.addChips(chipsArrayLabel);
        }


        mChipCloud.setDeleteListener((index, label) -> {
            Log.d(TAG, String.format("chipDeleted at index:%d  label:%s ", index, label));
            sendOutgoingQuickMessage(message, index, Message.TYPE_MESSAGE);

            mMessagesAdapter.addToStart(MessageUtils.getTextMessage(TEMP_USER_ID,
                    TEMP_USER_NAME,
                    label), true);

            mChipCloud.deselectIndex(index);
            deleteAllChipsWithAnimation();
            showTypingIndicator();
        });
    }

    private void deleteAllChipsWithAnimation() {
        final FlexboxLayout parent = mFlexBox;
        AlphaAnimation anim = new AlphaAnimation(1.0F, 0.0F);
        anim.setDuration(500);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (parent.getChildCount() > 0) {
                    parent.removeAllViews();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        parent.startAnimation(anim);
    }

    private void sendOutgoingMessage(String text) {
        if (mChatBot.isOpenWebSocket()) {
            mChatBot.sendMessage(MessageUtils.createOutgoingMessage(TEMP_USER_NAME,
                    TEMP_USER_ID, text));
        } else {
            mChatBot.connectToWebSocket();
            if (mChatBot.isOpenWebSocket()) {
                mChatBot.sendMessage(MessageUtils.createOutgoingMessage(TEMP_USER_NAME,
                        TEMP_USER_ID, text));
            }
        }

    }

    private void sendOutgoingQuickMessage(IncomingMessage message, int index, String messageType) {
        if (mChatBot.isOpenWebSocket()) {
            mChatBot.sendMessage(MessageUtils.createOutgoingQuickMessage(message, index));
        } else {
            mChatBot.connectToWebSocket();
            if (mChatBot.isOpenWebSocket()) {
                mChatBot.sendMessage(MessageUtils.createOutgoingQuickMessage(message, index));
            }
        }
    }
}