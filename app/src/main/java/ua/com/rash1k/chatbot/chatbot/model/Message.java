package ua.com.rash1k.chatbot.chatbot.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.stfalcon.chatkit.commons.models.IMessage;
import com.stfalcon.chatkit.commons.models.IUser;
import com.stfalcon.chatkit.commons.models.MessageContentType;

import java.util.Date;

import ua.com.rash1k.chatbot.chatbot.fixtures.MessagesFixtures;
import ua.com.rash1k.chatbot.chatbot.utils.MessageUtils;


public class Message implements IMessage,
        MessageContentType.Image /*this is for default image messages implementation*/ {


    public static final String TYPE_MESSAGE = "message";
    public static final String TYPE_TYPING = "typing";
    public static final String TYPE_HELLO = "hello";

    public static final String CONTENT_TYPE_TEXT = "text";
    public static final String CONTENT_TYPE_PICTURE = "picture"; // TODO get real
    public static final String CONTENT_TYPE_GIF = "gif"; // TODO get real

    @Expose(serialize = false, deserialize = false)
    transient private String messageId;

    //@Expose(serialize = false, deserialize = false)
//  transient private String text;
    @Expose(serialize = true, deserialize = true)
    transient private Image image;
    @Expose(serialize = false, deserialize = false)
    transient private Voice voice;
    @Expose(serialize = false, deserialize = false)
    transient private Date createdAt;


    @SerializedName("url")
    private String imageUrl;

    @SerializedName("text")
    private String text;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("user")
    @Expose
    private String userStr;

    @SerializedName("channel")
    @Expose
    private String channel;

    @SerializedName("user_profile")
    @Expose
    private IUser user;


    public Message() {
    }

    public Message(String userId, String userName) {
        this(MessagesFixtures.getRandomId(), MessageUtils.getUserProfile(userId, userName));
        this.userStr = userName;
    }

    public Message(String messageId, IUser user) {
        this.messageId = messageId;
        this.user = user;
    }

    public Message(String id, IUser user, String text) {
        this(id, user, text, new Date());
    }

    public Message(String id, IUser user, String text, Date createdAt) {
        this.messageId = id;
        this.text = text;
        this.user = user;
        this.createdAt = createdAt;
    }

    @Override
    public String getId() {
        return messageId;
    }

    public void setId(String id) {
        this.messageId = id;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public Date getCreatedAt() {
        return createdAt;
    }

    @Override
    public IUser getUser() {
        return this.user;
    }

    @Override
    public String getImageUrl() {
        return image == null ? null : image.getUrl();
    }

    public Voice getVoice() {
        return voice;
    }

    public void setVoice(Voice voice) {
        this.voice = voice;
    }

    public String getStatus() {
        return "Sent";
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Image getImage() {
        return image;
    }


    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getTextToBotKit() {
        return text;
    }

    public void setTextToBotKit(String textToBotKit) {
        this.text = textToBotKit;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUserStr() {
        return userStr;
    }

    public void setUserStr(String userStr) {
        this.userStr = userStr;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }


    public static class Image{
        @SerializedName("url")
        private String url;

        public Image(String url) {
            this.url = url;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

    public static class Voice {

        private String url;
        private int duration;

        public Voice(String url, int duration) {
            this.url = url;
            this.duration = duration;
        }

        public String getUrl() {
            return url;
        }

        public int getDuration() {
            return duration;
        }
    }


}

